/*
 * Messages.h
 *
 *  Created on: Feb 19, 2021
 *      Author: tom
 */

#ifndef APPLICATION_USER_CORE_MESSAGES_H_
#define APPLICATION_USER_CORE_MESSAGES_H_

#include "etl/message.h"
#include "etl/string.h"
#include <type_traits>

struct IntMessage: public etl::message<1>
{
  IntMessage(int intVal) :
    m_intval(intVal)
  {
  }

  int m_intval;
};

struct FloatMessage: public etl::message<2>
{
  FloatMessage(float floatVal) :
    floatVal(floatVal)
  {
  }

  float floatVal;
};

struct StringMessage: public etl::message<3>
{
  StringMessage(etl::string_view source) :
    stringVal(source)
  {
  }

  etl::string<32> stringVal;
};

#endif /* APPLICATION_USER_CORE_MESSAGES_H_ */
