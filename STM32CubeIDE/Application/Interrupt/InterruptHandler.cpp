// ---------------------------------------------------------------------------
//
// Filename:
// InterruptHandler.h
//
// Product or product-subsystem:
// Bentrons IRRCO10X irrigation system
//
// Original author:
// Tom Bulsink (Inspiro)
//
// Description:
// Simplified interrupt handler template based on ISL STM32 interrupt dispatcher
//
// ---------------------------------------------------------------------------
#include "InterruptHandler.h"
#include "FreeRTOS.h"

namespace BENTRONS
{
  template<IRQn_Type I>
  void HandleInterrupt()
  {
    bool handled = CInterruptHandler<I>::Handle();

    portYIELD_FROM_ISR(handled?pdTRUE:pdFALSE);
  }

  extern "C"
  {
  void EXTI15_10_IRQHandler(void)
  {
    HandleInterrupt<EXTI15_10_IRQn>();
  }
  }
}
