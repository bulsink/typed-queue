// ---------------------------------------------------------------------------
//
// Filename:
// InterruptHandler.h
//
// Product or product-subsystem:
// Bentrons IRRCO10X irrigation system
//
// Original author:
// Tom Bulsink (Inspiro)
//
// Description:
// Simplified interrupt handler template based on ISL STM32 interrupt dispatcher
//
// ---------------------------------------------------------------------------

#ifndef INFRA_INTERRUPTDISPATCHER_H_
#define INFRA_INTERRUPTDISPATCHER_H_

#include "stm32g4xx.h"

template<IRQn_Type I>
class CInterruptHandler
{
public:
  CInterruptHandler()
  {
    m_pHandler = this;
  }

  virtual bool Interrupt(const CInterruptHandler& handler) = 0;

  static bool Handle()
  {
    return m_pHandler && m_pHandler->Interrupt(*m_pHandler);
  }

protected:
  void Enable(uint32_t priority)
  {
    NVIC_SetPriority(I, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), priority, 0));
    NVIC_EnableIRQ(I);
  }

private:

  static CInterruptHandler<I>* m_pHandler;

};

template<IRQn_Type I>
CInterruptHandler<I>* CInterruptHandler<I>::m_pHandler = nullptr;

#endif // INFRA_INTERRUPTDISPATCHER_H_
