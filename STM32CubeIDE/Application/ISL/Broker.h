/*
 * Broker.h
 *
 *  Created on: Feb 20, 2021
 *      Author: tom
 */

#ifndef APPLICATION_USER_CORE_BROKER_H_
#define APPLICATION_USER_CORE_BROKER_H_

#include "etl/message_bus.h"

class IBroker
{
public:
  virtual bool Subscribe(etl::imessage_router& rMessageRouter) = 0;
  virtual void Publish(const etl::imessage& rMessage) = 0;
};

template<etl::message_id_t MAX_SUBSCRIBERS>
class CBroker: private etl::message_bus<MAX_SUBSCRIBERS>, public IBroker
{
  using MessageBus_t = etl::message_bus<MAX_SUBSCRIBERS>;

public:
  bool Subscribe(etl::imessage_router& rMessageRouter) override
  {
    return MessageBus_t::subscribe(rMessageRouter);
  }

  void Publish(const etl::imessage& rMessage) override
  {
    MessageBus_t::receive(rMessage);
  }
};



#endif /* APPLICATION_USER_CORE_BROKER_H_ */
