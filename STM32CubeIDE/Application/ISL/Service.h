/*
 * Service.h
 *
 *  Created on: Feb 20, 2021
 *      Author: tom
 */

#ifndef APPLICATION_USER_CORE_SERVICE_H_
#define APPLICATION_USER_CORE_SERVICE_H_

#include "FreeRTOS.h"
#include "cmsis_os2.h"
#include "etl/message_router.h"
#include "etl/delegate.h"
#include "etl/queue.h"

using InvokeMethod_t = etl::delegate<void()>;

struct InvokeMessage: public etl::message<255>
{
  InvokeMessage(const InvokeMethod_t& del) :
      del { del }
  {
  }

  InvokeMethod_t del;
};

extern "C"
{
static inline
void TaskRunner(void* args)
{
  if (auto pDelegate = static_cast<InvokeMethod_t*>(args))
  {
    (*pDelegate)();
  }
}
} // extern "C"

//
//  In a critical section:
//  A A task cannot be pre-empted by other tasks
//  B A task cannot be interrupted *by interrupts that also access the queue*
//  Note that there may still be high priority interrupts coming in as interrupts with
//  higher than configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY will still be serviced
static inline
void osEnterCritical()
{
  portENTER_CRITICAL();
}

static inline
void osExitCritical()
{
  portEXIT_CRITICAL();
}

template<typename TService, typename ... TMessages>
class CServiceBase: public etl::message_router<TService, InvokeMessage, TMessages...>
{
public:
  const static auto IDLE_TIME_MS = osWaitForever;
  const static auto QUEUE_SIZE = 5;
  using MessageRouter_t = etl::message_router<TService, InvokeMessage, TMessages...>;

  using MessageRouter_t::receive;

  CServiceBase(IBroker& rBroker) :
      MessageRouter_t(0),
      m_rBroker { rBroker },
      m_taskDelegate { InvokeMethod_t::create<CServiceBase, &CServiceBase::Process >(*this) }
  {
    auto mutexAttr = osMutexAttr_t { .name = "SVCMUT" };
    m_mutexId = osMutexNew(&mutexAttr);
    assert(m_mutexId);

    auto semAttr = osSemaphoreAttr_t { .name = "SVCSEM", };
    m_readerSemId = osSemaphoreNew(QUEUE_SIZE, 0, &semAttr);
    assert(m_readerSemId);

    rBroker.Subscribe(*this);
  }

  virtual void Start(uint32_t stackSize, osPriority_t priority, const char* name = "SVC")
  {
    auto attr = osThreadAttr_t
    {
      .name = name,
      .attr_bits = 0,
      .cb_mem = 0,
      .cb_size = 0,
      .stack_mem = 0,
      .stack_size = stackSize,
      .priority = priority
    };

    m_threadId = osThreadNew(TaskRunner, &m_taskDelegate, &attr);
    assert(m_threadId);
  }

  void Process()
  {
    while (true)
    {
      // I'm a reader, wait for a message
      if (osSemaphoreAcquire(m_readerSemId, IDLE_TIME_MS) == osOK)
      {
        // Lock out lower priority interrupts
        osEnterCritical();
        assert(!m_queue.empty());
        auto item = m_queue.front();
        m_queue.pop();
        osExitCritical();

        // And process it.
        MessageRouter_t::receive(item.get());
      }
      else
      {
        OnIdle();
      }
    }
  }

  void receive(const etl::imessage& msg) override
  {
    if (MessageRouter_t::accepts(msg))
    {
      bool success = false;
      do
      {
        // Lock out lower priority interrupts
        osEnterCritical();
        success = !m_queue.full();
        if (success)
        {
          m_queue.emplace(msg);
        }
        osExitCritical();

        if (success)
        {
          // Signal reader
          osSemaphoreRelease(m_readerSemId);
        }
        else
        {
          OnQueueFull();
        }

      } while (!success);
    }
  }

  void on_receive(const InvokeMessage& msg)
  {
    msg.del();
  }

  template<typename TMessage>
  void on_receive(const TMessage& msg)
  {
    static_cast<TService*>(this)->Handle(msg);
  }

  void on_receive_unknown(const etl::imessage& msg)
  {
  }

protected:
  template<typename TMessage>
  void Publish(const TMessage& message)
  {
    m_rBroker.Publish(message);
  }

  void Invoke(const InvokeMethod_t& del)
  {
    receive(InvokeMessage { del });
  }

  // Cannot be called from higher priority interrupts
  void InvokeFromIsr(const InvokeMethod_t& del)
  {
    if (!m_queue.full())
    {
      m_queue.emplace(InvokeMessage { del });
      osSemaphoreRelease(m_readerSemId);
    }
  }

  virtual void OnIdle()
  {
  }

  virtual void OnQueueFull()
  {
    osDelay(1); // Relinquish time slice
  }

  IBroker& m_rBroker;

private:
  osMutexId_t m_mutexId;
  osSemaphoreId_t m_readerSemId;
  osThreadId_t m_threadId;
  InvokeMethod_t m_taskDelegate;
  etl::queue<etl::message_packet<InvokeMessage, TMessages...>, QUEUE_SIZE> m_queue;
};

#endif /* APPLICATION_USER_CORE_SERVICE_H_ */
