/*
 * Test.cpp
 *
 *  Created on: Feb 20, 2021
 *      Author: tom
 */
#include "Broker.h"
#include "Service.h"
#include "Messages.h"
#include "../Interrupt/InterruptHandler.h"

#include "FreeRTOS.h"
#include "timers.h"
#include"cmsis_os2.h"

#include "main.h"
#include "stm32g4xx_ll_gpio.h"

class Subscriber;
using SubscriberBase = CServiceBase<Subscriber, StringMessage>;
class Subscriber: public SubscriberBase
{
  using SubscriberBase::CServiceBase;

public:
  void OnIdle()
  {
  }

  void Handle(const StringMessage& message)
  {
    static_assert(!std::is_trivially_copyable<StringMessage>::value);
    LL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
  }
};

class Publisher;
using PublisherBase = CServiceBase<Publisher, FloatMessage, IntMessage>;
class Publisher: public PublisherBase, public CInterruptHandler<IRQn_Type::EXTI15_10_IRQn>
{
  using PublisherBase::CServiceBase;
public:
  void Start(uint32_t stackSize, osPriority_t priority, const char* name) override
  {
    PublisherBase::Start(stackSize, priority, name);
    Enable(configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY);
  }

  void OnIdle()
  {
  }

  void Handle(const IntMessage& message)
  {
  }

  void Handle(const FloatMessage& message)
  {
    LL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
  }

  void ButtonPressed()
  {
    Publish(StringMessage{ "another message"});
  }

  bool Interrupt(const CInterruptHandler&) override
  {
    if (LL_EXTI_IsActiveFlag_0_31(LL_EXTI_LINE_13) != RESET)
    {
      LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_13);
      InvokeFromIsr(InvokeMethod_t::create<Publisher, &Publisher::ButtonPressed>(*this));
    }

    return true;
  }
};

CBroker<10> broker;
Subscriber subscriber(broker);
Publisher publisher(broker);

extern "C"
{
void StartDefaultTask(void* argument)
{
  subscriber.Start(1024, osPriority_t::osPriorityAboveNormal, "SUB");
  publisher.Start(1024, osPriority_t::osPriorityNormal, "PUB");

  for (;;)
  {
    broker.Publish(StringMessage { "non trivially copyable message" });
    osDelay(rand() % 10);
  }
  /* USER CODE END 5 */
}
}
